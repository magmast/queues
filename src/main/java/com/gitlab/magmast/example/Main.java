package com.gitlab.magmast.example;

import com.gitlab.magmast.queues.MergeStrategy;
import com.gitlab.magmast.queues.Queue;
import com.gitlab.magmast.queues.QueueExecutor;
import com.gitlab.magmast.queues.XmlResourceQueueSource;

public class Main {
    public static void main(String... args) {
        final var queueExecutor = new QueueExecutor(MergeStrategy.MERGE);
        queueExecutor.addFromSource(new XmlResourceQueueSource("queues.xml"));
        queueExecutor.addQueue("GameStart", Queue.of(new LoggerAction("Game started")));
        queueExecutor.executeQueue("GameStart", null);
    }
}
