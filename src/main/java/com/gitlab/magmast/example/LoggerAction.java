package com.gitlab.magmast.example;

import com.gitlab.magmast.queues.Action;
import com.gitlab.magmast.queues.ExecutionContext;

public class LoggerAction implements Action {
    private final String message;

    public LoggerAction(String message) {
        this.message = message;
    }

    @Override
    public void execute(ExecutionContext context) {
        System.out.println(message);
    }
}
