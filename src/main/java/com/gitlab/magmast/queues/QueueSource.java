package com.gitlab.magmast.queues;

import java.util.Map;

public interface QueueSource {
    Map<String, Queue> read();
}
