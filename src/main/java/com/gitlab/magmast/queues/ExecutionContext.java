package com.gitlab.magmast.queues;

public class ExecutionContext {
    private final Object value;

    public ExecutionContext(Object value) {
        this.value = value;
    }

    public Object get() {
        return value;
    }

    public <T> T as() {
        return (T) value;
    }
}
