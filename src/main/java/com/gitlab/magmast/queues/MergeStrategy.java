package com.gitlab.magmast.queues;

public enum MergeStrategy {
    /**
     * If queue with the same name already exist, new queues are being ignored.
     */
    IGNORE,

    /**
     * Any new queue with the same name overrides old queue.
     */
    OVERRIDE,

    /**
     * Queues with the same name are joined.
     */
    MERGE,
}
