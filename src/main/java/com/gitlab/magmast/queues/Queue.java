package com.gitlab.magmast.queues;

import java.util.List;
import java.util.stream.Stream;

public class Queue {
    private final List<Action> actions;

    private Queue(List<Action> actions) {
        this.actions = actions;
    }

    public static Queue of(Action... actions) {
        return new Queue(List.of(actions));
    }

    /**
     * Joins queues together and returns a new queue.
     * @return A new queue containing actions of both queues.
     */
    public Queue join(Queue other) {
        return new Queue(Stream.concat(actions.stream(), other.actions.stream()).toList());
    }

    public void execute(Object contextValue) {
        final var context = new ExecutionContext(contextValue);
        actions.forEach(action -> action.execute(context));
    }
}
