package com.gitlab.magmast.queues;

public interface Action {
    void execute(ExecutionContext context);
}
