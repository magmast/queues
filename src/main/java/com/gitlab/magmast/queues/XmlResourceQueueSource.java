package com.gitlab.magmast.queues;

import java.util.Map;

public class XmlResourceQueueSource implements QueueSource {
    private final String resourceName;

    public XmlResourceQueueSource(String resourceName) {
        this.resourceName = resourceName;
    }

    @Override
    public Map<String, Queue> read() {
        throw new RuntimeException("Not implemented");
    }
}
