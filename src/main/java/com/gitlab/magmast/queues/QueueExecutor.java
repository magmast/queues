package com.gitlab.magmast.queues;

import java.util.HashMap;
import java.util.Map;

public class QueueExecutor {
    private final Map<String, Queue> queues = new HashMap<>();

    private final MergeStrategy defaultMergeStrategy;

    public QueueExecutor(MergeStrategy defaultMergeStrategy) {
        this.defaultMergeStrategy = defaultMergeStrategy;
    }

    public QueueExecutor() {
        this(MergeStrategy.OVERRIDE);
    }

    public void addQueue(String name, Queue queue, MergeStrategy mergeStrategy) {
        switch (mergeStrategy) {
            case OVERRIDE -> queues.put(name, queue);
            case IGNORE -> queues.putIfAbsent(name, queue);
            case MERGE -> {
                if (queues.containsKey(name)) {
                    final var existingQueue = queues.get(name);
                    queues.put(name, existingQueue.join(queue));
                } else {
                    queues.put(name, queue);
                }
            }
        }
    }

    public void addQueue(String name, Queue queue) {
        addQueue(name, queue, defaultMergeStrategy);
    }

    public void addFromSource(QueueSource queueSource, MergeStrategy mergeStrategy) {
        queueSource.read().forEach((name, queue) -> addQueue(name, queue, mergeStrategy));
    }

    public void addFromSource(QueueSource queueSource) {
        addFromSource(queueSource, defaultMergeStrategy);
    }

    public void executeQueue(String name, Object contextValue) {
        final var queue = queues.get(name);
        queue.execute(contextValue);
    }
}
